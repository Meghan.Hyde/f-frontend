function createCard(name, description, pictureUrl, location, startDate, endDate) { //this is getting the image for the location for each conference


    return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        <p>${startDate} - ${endDate}</p>
        </div>
      </div>
    `;
  }


function createAlert(alertType, message) {
    return `
    <div class="alert alert-${alertType}" role="alert">
    ${message}
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) { // this error is thrown when you have a 400 error or something within your code is wrong but URL is correct
            //something goes here
            // throw new Error('Response not OK');
            const error = response.status;
            const message = `Warning - Response not OK. Status ${error}`;
            const alertType = 'warning';
            const alertHtml = createAlert(alertType, message);
            const alertDiv = document.getElementById('alert-div');
            alertDiv.innerHTML = alertHtml;

        } else {
            const data = await response.json();

            for (let conference of data.conferences){
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    // console.log(details);
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const start = details.conference.starts;
                    const end = details.conference.ends;
                    const parsedStart = Date.parse(start);
                    const parsedEnd = Date.parse(end);
                    const startDate = new Date(parsedStart).toLocaleDateString();
                    const endDate = new Date(parsedEnd).toLocaleDateString();

                    const location = details.conference.location.name;

                    const html = createCard(name, description, pictureUrl, location, startDate, endDate);
                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                    // const description = document.querySelector('.card-text');
                    // description.innerHTML = details.conference.description; //calling details from the JSON response variable above
                    // console.log(description);
                    // console.log(details);
                    // const imageTag = document.querySelector('.card-img-top');
                    // imageTag.src = details.conference.location.picture_url;
                }

            }

            // // console.log(conference);
            // const nameTag = document.querySelector('.card-title'); //name of conference in h5 tag index.html
            // nameTag.innerHTML = conference.name; // using query selctor to reference that tag and calling the class card-title
            //     // then you set the innerHTML property of the element


        }
    } catch (error) {
        //something goes here
        console.error('error', error); // this error is when your URL is off

        const alertType = 'danger';
        const message = 'Site failed to load';
        const dangerHtml = createAlert*(alertType, message);
        const alertDiv = document.getElementById('alert-div');
        alertDiv.innerHTML = dangerHtml;
    }


});
