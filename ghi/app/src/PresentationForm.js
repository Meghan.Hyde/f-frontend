import React, { useState, useEffect } from "react";

function PresentationForm ({conferences}) { //destructuring keys off a prop object instead of calling props over and over

    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    // const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('');

    const handlePresenterNameChange = (event) => {
        setPresenterName(event.target.value);
    }

    const handlePresenterEmailChange = (event) => {
        setPresenterEmail(event.target.value);
    }

    const handleCompanyNameChange = (event) => {
        setCompanyName(event.target.value);
    }

    const handleTitleChange = (event) => {
        setTitle(event.target.value);
    }

    const handleSynopsisChange = (event) => {
        setSynopsis(event.target.value);
    }

    const handleConferenceChange = (event) => {
        setConference(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.synopsis = synopsis;
        data.synopsis = synopsis;
        data.conference = conference;
        console.log('Submitting data:', data);

        // const conferenceID = data.conference
        console.log('Conference:', conference)


    const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
            },
        };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
        const newPresentation = await response.json();
        console.log('New Presentation:', newPresentation);
        //clear form fields
        setPresenterName('');
        setPresenterEmail('');
        setCompanyName('');
        setTitle('');
        setSynopsis('');
        setConference('');


        }
    };



    // const fetchData = async () => {
    //     const conferenceUrl = 'http://localhost:8000/api/conferences/';

    //     const response = await fetch(conferenceUrl);

    //     if (response.ok) {
    //         const data = await response.json();
    //         setConferences(data.conferences);


    //     }
    // };

    // useEffect(() => {
    //     fetchData();
    // }, [] );


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Presentation</h1>
            <form onSubmit={handleSubmit}
            id="create-presentation-form">

                <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange}
                placeholder="Presenter name"
                required type="text"
                name="presenter_name"
                id="presenter_name"
                value={presenterName}
                className="form-control" />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange}
                placeholder="Presenter email"
                required type="email"
                name="presenter_email"
                id="presenter_email"
                value={presenterEmail}
                className="form-control" />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange}
                placeholder="Company name"
                required type="text"
                name="company_name"
                id="company_name"
                value={companyName}
                className="form-control" />
                <label htmlFor="company_name">Company Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleTitleChange}
                placeholder="title"
                required type="text"
                name="title"
                id="title"
                value={title}
                className="form-control" />
                <label htmlFor="title">Job Title</label>
              </div>

              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange}
                placeholder=""
                required type="text"
                name="synopsis"
                id="synopsis"
                rows="3"
                value={synopsis}
                className="form-control"></textarea>
              </div>

              <div className="mb-3">
                <select onChange={handleConferenceChange}
                required name="conference"
                id="conference"
                value={conference}
                className="form-select">
                  <option value="">Choose Conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default PresentationForm;
