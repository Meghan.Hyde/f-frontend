import React, { useEffect, useState } from "react";

function LocationForm(props) {

    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const[city, setCity] = useState('');
    const [states, setStates] = useState([]); // array of all states
    const [state, setState] = useState(''); // choice of state from dropdown

    const handleNameChange = (event) => {
        setName(event.target.value);
    }

    const handleRoomCountChange = (event) => {
        setRoomCount(event.target.value);
    }

    const handleCityChange = (event) => {
        setCity(event.target.value);
    }

    const handleStateChange = (event) => {
        setState(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name; // data.xxx is the name that shows up in Insomnia
        data.room_count = roomCount;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);


            setName('');
            setRoomCount('');
            setCity('');
            setState(''); //to clear form on save, you need to set the state to empty strings and use those valuesfor the form elements

        }


        };


    const fetchData = async () => {

        const stateUrl = 'http://localhost:8000/api/states/';

        const response = await fetch(stateUrl);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);


        }
    };


    useEffect(() => {
        fetchData();
    }, [] );


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Location</h1>
            <form onSubmit={handleSubmit}
            id="create-location-form">

                <div className="form-floating mb-3">
                <input onChange={handleNameChange}
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                value={name}
                className="form-control" />
                <label htmlFor="name">Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleRoomCountChange}
                placeholder="Room count"
                required type="number"
                name="room_count"
                id="room_count"
                value={roomCount} //add this when you add the end of the submit function and set each value to an empty string
                className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleCityChange}
                placeholder="City"
                required type="text"
                name="city"
                id="city"
                value={city}
                className="form-control" />
                <label htmlFor="city">City</label>
              </div>

              <div className="mb-3">
                <select onChange={handleStateChange}
                required name="state"
                id="state"
                value={state}
                className="form-select">
                  <option value="">Choose A State</option>
                  {states.map(state => {
                    // console.log('State', state);
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default LocationForm;
