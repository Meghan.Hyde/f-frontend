import logo from './logo.svg';
import { useEffect, useState } from 'react';
import './App.css';
import Nav from './Nav';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  const [ attendees, setAttendees] = useState([]);
  const [conferences, setConferences] = useState([]);

  async function getAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');
    if (response.ok) {
      const { attendees } = await response.json(); // getting data
      setAttendees(attendees); // setting data
  } else {
    console.error('An error occured when fetching the data for attendees')
    }
  }

  async function getConferences() {
    const conferenceUrl = 'http://localhost:8000/api/conferences/';

    const response = await fetch(conferenceUrl);

    if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);


    }
};

// useEffect(() => {
//     fetchData();
// }, [] );


// useEffect will call the getAttendees function and update baased on API.
//will not work if there is a bug in code. ONly after React app gets mounted on screen
  useEffect(() =>  {
    getAttendees();
    getConferences();
  }, [])




  if (attendees === undefined) {
    // was getting an error calling length since react.strictmode was not passing anything through <App/>
    return null;
  }

  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route index element={<MainPage conferences={conferences}/>} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm getConferences={getConferences}/>} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm conferences={conferences}/>} />
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm conferences={conferences} getAttendees={getAttendees} />} />
          <Route index element={<AttendeesList attendees={attendees} />} />
        </Route>

      </Routes>
      {/* <AttendConferenceForm /> */}
      {/* <ConferenceForm /> */}
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}

    </div>
    </BrowserRouter>
  );
}

export default App;

//props.attendees (attendees is the variable name that matches index.js. only data.attendees is actually tapping in to
//the data in Insomnia for attendees list)


