import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// <App /> is what runs the app from App.js

// async function loadAttendees() {
//   const response = await fetch('http://localhost:8001/api/attendees/');
//   // console.log(response);

//   if(response.ok) {
//     const data = await response.json();
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
    );
//     // console.log(data);
//   } else {
//     console.error(response);
//   }
// }
// loadAttendees();

// two reasons why incorrect on line 30 =
//line 30 code is props but we don't need prop as it is an anti-pattern. This is state and should be handled as state within app.
// react will automatically render things as state changes
//if you need to load data from an API you do it through UseEffect()


// React StrictMode lets you find common bugs in your components during development

//  <App attendees={data.attendees}/>, sets a variable named 'attendees' to the value
//inside the curly braces. Then, that variable becomes a property on the props parameter that gets
//passed into the function. He points to the line drawn between the "attendees" at the top of the drawing
// to the "props.attendees" inside the curly braces on the bottom. "That's how you end up passing arguments
//to the React functions. They call them components, by the way, the functions in React that return JSX."
